import 'dart:html';
import 'dart:math';

void main() {
  querySelector("#play-btn").onClick.listen(play);
  querySelector("#gen-btn").onClick.listen(generate);
}

void play(MouseEvent event) {
  // Sequances 
  InputElement player1Elem = querySelector("#player1");
  String player1 = player1Elem.value.toUpperCase();
  InputElement player2Elem = querySelector("#player2");
  String player2 = player2Elem.value.toUpperCase();
  
  if(player1 == player2) {
    querySelector("#results").text = "Players can't have the same sequance";
    return;
  } else if(player1.isEmpty || player2.isEmpty) {
    querySelector("#results").text = "Both players must fill in a sequance";
    return;
  }
  
  InputElement iElem = querySelector("#rounds");
  int rounds = int.parse(iElem.value);
  
  String fullSeq = "";
  int pos1 = 0;
  int pos2 = 0;
  int score1 = 0;
  int score2 = 0;
  int ties = 0;
  for(int i = 0; i < rounds; i++) {
     fullSeq = initFullSeq(rounds);
     pos1 = fullSeq.indexOf(player1);
     pos2 = fullSeq.indexOf(player2);
     
     if(pos1 == -1 && pos2 == -1) {
       ties++;
     } else if(pos2 == -1 || pos1 > pos2) {
       score1++;
     } else if (pos1 == -1 || pos2 > pos1){
       score2++;
     } else {
       print("Error...");
     }
  }
  
  String text = "";
  if(score1 > score2) {
    text = "Player 1 wins! ($score1/$rounds rounds)";
  } else if(score2 > score1) {
    text = "Player 2 wins! ($score2/$rounds rounds)";
  } else {
    text = "Tie! $score1 vs $score2";
  }
  querySelector("#results").text = text;  
}

void generate(MouseEvent event) {
  InputElement seqElem = querySelector("#seq");
  String seq = seqElem.value.toUpperCase();
  InputElement iElem = querySelector("#length");
  int tosses = int.parse(iElem.value);
  String fullSeq = initFullSeq(tosses);
  querySelector("#seq-cont").children = colorSeqs(seq, fullSeq );
  
  querySelector("#matches").text = "Matches: " + countSeq(seq, fullSeq).toString();  
}

String initFullSeq([length = 100]) {
  String sequence = "";
  var rand = new Random();
  for (var i = 0; i < length; i++) {
    if(rand.nextBool()) {
      sequence += "H";
    } else {
      sequence += "T";
    }
  }
  return sequence;
}

int countSeq(String seq, String fullSeq) {
  int count = 0;
  for(int i = 0; i < fullSeq.length - seq.length; i++) {
    String testSeq = fullSeq.substring(i, i+seq.length);
    if(testSeq == seq) {
      count++;
    }
  }
  return count;
}

List<Element> colorSeqs(String seq, String fullSeq) {
  List<Element> elemList = [];
  int seqPos = fullSeq.indexOf(seq, 0);
  String tmpFullSeq = fullSeq;
  while(seqPos != - 1) {
    
    if(tmpFullSeq.length <= seq.length) {
      elemList.add(newMatchElem(tmpFullSeq, false));
      break;
    }
    
    seqPos = tmpFullSeq.indexOf(seq, 0); 
    
    if(seqPos == -1) {
      elemList.add(newMatchElem(tmpFullSeq, false));
      break;
    }
    //Save and remove first none matching letters
    elemList.add(newMatchElem(tmpFullSeq.substring(0, seqPos), false));
    tmpFullSeq = tmpFullSeq.substring(seqPos, tmpFullSeq.length);
    
    //Save and remove matching letters
    int pos = getNoMatchPos(seq, tmpFullSeq);
    elemList.add(newMatchElem(tmpFullSeq.substring(0, pos), true));
    tmpFullSeq = tmpFullSeq.substring(pos, tmpFullSeq.length);
  }
  return elemList;
}

int getNoMatchPos(String seq, String fullSeq) {
  int endPos = 0;
  int i = 0;
  do {
    if(i + seq.length > fullSeq.length) {
      break;
    }
    String string = fullSeq.substring(i, seq.length + i);
    if(fullSeq.substring(i, seq.length + i) == seq) {
      endPos = i + seq.length;
    }
    i++;
  } while(i < endPos);
  return endPos;
}

Element newMatchElem(String text, bool match) {
  Element elem = new SpanElement();
  elem.text = text;
  if(match) {
    elem.classes.add('match');
  } else {
    elem.classes.add('no-match');
  }
  return elem;
}


